package user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Stream;

import com.andreea.grpc.User.APIResponse;
import com.andreea.grpc.User.Empty;
import com.andreea.grpc.User.Medication;
import com.andreea.grpc.User.TreatmentRequest;
import com.andreea.grpc.userGrpc.userImplBase;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import io.grpc.stub.StreamObserver;

public class UserService extends userImplBase {

	@Override
	public void getTreatment(Empty request, StreamObserver<TreatmentRequest> responseObserver) {
		Medication medication1 = Medication.newBuilder().setName("propolis").setInterval1(01).setInterval2(02).setTaken(false).build();
		Medication medication2 = Medication.newBuilder().setName("algocalmin").setInterval1(10).setInterval2(12).setTaken(false).build();
		Medication medication3 = Medication.newBuilder().setName("nurofen").setInterval1(20).setInterval2(22).setTaken(false).build();
		Medication medication4 = Medication.newBuilder().setName("nospa").setInterval1(12).setInterval2(14).setTaken(false).build();
		Medication medication5 = Medication.newBuilder().setName("vitamina C").setInterval1(16).setInterval2(25).setTaken(false).build();
		
		TreatmentRequest.Builder treatment = TreatmentRequest.newBuilder().addMedication(medication1).addMedication(medication2).addMedication(medication3).addMedication(medication4).addMedication(medication5);
		
		responseObserver.onNext(treatment.build());
		responseObserver.onCompleted();
	}

	@Override
	public void informServerOfTakenMedications(Medication request, StreamObserver<APIResponse> responseObserver) {
		String name = request.getName();
		boolean taken = request.getTaken();
		APIResponse.Builder response = APIResponse.newBuilder();
		
		if(taken) {
			response.setResponsecode(0).setResponsemessage("The user has taken "+name+" in the right interval!");
		}else if(!taken) {
			response.setResponsecode(0).setResponsemessage("The user has NOT taken "+name+" in the right interval!");
		}
		
		responseObserver.onNext(response.build());
		responseObserver.onCompleted();
	}

	@Override
	public void generateFile(TreatmentRequest request, StreamObserver<Empty> responseObserver){
		
		 try {
		      File myObj = new File("filename.txt");
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		 
		 try {
		      FileWriter myWriter = new FileWriter("filename.txt");
		      myWriter.write("Files in Java might be tricky, but it is fun enough!");
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		  }
}

	

	/*@Override
	public void login(LoginRequest request, StreamObserver<APIResponse> responseObserver) {
		
		System.out.println("inside login\n");
		String username = request.getUsername();
		String password = request.getPassword();
		
		APIResponse.Builder response = APIResponse.newBuilder();
		if(username.equals(password)) {
			response.setResponsecode(0).setResponsemessage("SUCCESFUL LOGIN!");
		}else {
			response.setResponsecode(100).setResponsemessage("WRONG CREDENTIALS!");
		}
		
		responseObserver.onNext(response.build());
		responseObserver.onCompleted();
	
	}

	@Override
	public void logout(Empty request, StreamObserver<APIResponse> responseObserver) {
		// TODO Auto-generated method stub
		super.logout(request, responseObserver);
	}*/