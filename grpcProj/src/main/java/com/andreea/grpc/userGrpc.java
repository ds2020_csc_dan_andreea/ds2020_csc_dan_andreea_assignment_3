package com.andreea.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: user.proto")
public final class userGrpc {

  private userGrpc() {}

  public static final String SERVICE_NAME = "user";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.andreea.grpc.User.Empty,
      com.andreea.grpc.User.TreatmentRequest> getGetTreatmentMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getTreatment",
      requestType = com.andreea.grpc.User.Empty.class,
      responseType = com.andreea.grpc.User.TreatmentRequest.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.andreea.grpc.User.Empty,
      com.andreea.grpc.User.TreatmentRequest> getGetTreatmentMethod() {
    io.grpc.MethodDescriptor<com.andreea.grpc.User.Empty, com.andreea.grpc.User.TreatmentRequest> getGetTreatmentMethod;
    if ((getGetTreatmentMethod = userGrpc.getGetTreatmentMethod) == null) {
      synchronized (userGrpc.class) {
        if ((getGetTreatmentMethod = userGrpc.getGetTreatmentMethod) == null) {
          userGrpc.getGetTreatmentMethod = getGetTreatmentMethod = 
              io.grpc.MethodDescriptor.<com.andreea.grpc.User.Empty, com.andreea.grpc.User.TreatmentRequest>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "user", "getTreatment"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.TreatmentRequest.getDefaultInstance()))
                  .setSchemaDescriptor(new userMethodDescriptorSupplier("getTreatment"))
                  .build();
          }
        }
     }
     return getGetTreatmentMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.andreea.grpc.User.Medication,
      com.andreea.grpc.User.APIResponse> getInformServerOfTakenMedicationsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "informServerOfTakenMedications",
      requestType = com.andreea.grpc.User.Medication.class,
      responseType = com.andreea.grpc.User.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.andreea.grpc.User.Medication,
      com.andreea.grpc.User.APIResponse> getInformServerOfTakenMedicationsMethod() {
    io.grpc.MethodDescriptor<com.andreea.grpc.User.Medication, com.andreea.grpc.User.APIResponse> getInformServerOfTakenMedicationsMethod;
    if ((getInformServerOfTakenMedicationsMethod = userGrpc.getInformServerOfTakenMedicationsMethod) == null) {
      synchronized (userGrpc.class) {
        if ((getInformServerOfTakenMedicationsMethod = userGrpc.getInformServerOfTakenMedicationsMethod) == null) {
          userGrpc.getInformServerOfTakenMedicationsMethod = getInformServerOfTakenMedicationsMethod = 
              io.grpc.MethodDescriptor.<com.andreea.grpc.User.Medication, com.andreea.grpc.User.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "user", "informServerOfTakenMedications"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.Medication.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new userMethodDescriptorSupplier("informServerOfTakenMedications"))
                  .build();
          }
        }
     }
     return getInformServerOfTakenMedicationsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.andreea.grpc.User.TreatmentRequest,
      com.andreea.grpc.User.Empty> getGenerateFileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "generateFile",
      requestType = com.andreea.grpc.User.TreatmentRequest.class,
      responseType = com.andreea.grpc.User.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.andreea.grpc.User.TreatmentRequest,
      com.andreea.grpc.User.Empty> getGenerateFileMethod() {
    io.grpc.MethodDescriptor<com.andreea.grpc.User.TreatmentRequest, com.andreea.grpc.User.Empty> getGenerateFileMethod;
    if ((getGenerateFileMethod = userGrpc.getGenerateFileMethod) == null) {
      synchronized (userGrpc.class) {
        if ((getGenerateFileMethod = userGrpc.getGenerateFileMethod) == null) {
          userGrpc.getGenerateFileMethod = getGenerateFileMethod = 
              io.grpc.MethodDescriptor.<com.andreea.grpc.User.TreatmentRequest, com.andreea.grpc.User.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "user", "generateFile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.TreatmentRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.andreea.grpc.User.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new userMethodDescriptorSupplier("generateFile"))
                  .build();
          }
        }
     }
     return getGenerateFileMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static userStub newStub(io.grpc.Channel channel) {
    return new userStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static userBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new userBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static userFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new userFutureStub(channel);
  }

  /**
   */
  public static abstract class userImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *rpc login(LoginRequest) returns (APIResponse);
     *rpc logout(Empty) returns (APIResponse);
     * </pre>
     */
    public void getTreatment(com.andreea.grpc.User.Empty request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.TreatmentRequest> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTreatmentMethod(), responseObserver);
    }

    /**
     */
    public void informServerOfTakenMedications(com.andreea.grpc.User.Medication request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getInformServerOfTakenMedicationsMethod(), responseObserver);
    }

    /**
     */
    public void generateFile(com.andreea.grpc.User.TreatmentRequest request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getGenerateFileMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetTreatmentMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.andreea.grpc.User.Empty,
                com.andreea.grpc.User.TreatmentRequest>(
                  this, METHODID_GET_TREATMENT)))
          .addMethod(
            getInformServerOfTakenMedicationsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.andreea.grpc.User.Medication,
                com.andreea.grpc.User.APIResponse>(
                  this, METHODID_INFORM_SERVER_OF_TAKEN_MEDICATIONS)))
          .addMethod(
            getGenerateFileMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.andreea.grpc.User.TreatmentRequest,
                com.andreea.grpc.User.Empty>(
                  this, METHODID_GENERATE_FILE)))
          .build();
    }
  }

  /**
   */
  public static final class userStub extends io.grpc.stub.AbstractStub<userStub> {
    private userStub(io.grpc.Channel channel) {
      super(channel);
    }

    private userStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected userStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new userStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc login(LoginRequest) returns (APIResponse);
     *rpc logout(Empty) returns (APIResponse);
     * </pre>
     */
    public void getTreatment(com.andreea.grpc.User.Empty request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.TreatmentRequest> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTreatmentMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void informServerOfTakenMedications(com.andreea.grpc.User.Medication request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInformServerOfTakenMedicationsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void generateFile(com.andreea.grpc.User.TreatmentRequest request,
        io.grpc.stub.StreamObserver<com.andreea.grpc.User.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGenerateFileMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class userBlockingStub extends io.grpc.stub.AbstractStub<userBlockingStub> {
    private userBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private userBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected userBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new userBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc login(LoginRequest) returns (APIResponse);
     *rpc logout(Empty) returns (APIResponse);
     * </pre>
     */
    public com.andreea.grpc.User.TreatmentRequest getTreatment(com.andreea.grpc.User.Empty request) {
      return blockingUnaryCall(
          getChannel(), getGetTreatmentMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.andreea.grpc.User.APIResponse informServerOfTakenMedications(com.andreea.grpc.User.Medication request) {
      return blockingUnaryCall(
          getChannel(), getInformServerOfTakenMedicationsMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.andreea.grpc.User.Empty generateFile(com.andreea.grpc.User.TreatmentRequest request) {
      return blockingUnaryCall(
          getChannel(), getGenerateFileMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class userFutureStub extends io.grpc.stub.AbstractStub<userFutureStub> {
    private userFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private userFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected userFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new userFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc login(LoginRequest) returns (APIResponse);
     *rpc logout(Empty) returns (APIResponse);
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.andreea.grpc.User.TreatmentRequest> getTreatment(
        com.andreea.grpc.User.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTreatmentMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.andreea.grpc.User.APIResponse> informServerOfTakenMedications(
        com.andreea.grpc.User.Medication request) {
      return futureUnaryCall(
          getChannel().newCall(getInformServerOfTakenMedicationsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.andreea.grpc.User.Empty> generateFile(
        com.andreea.grpc.User.TreatmentRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGenerateFileMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_TREATMENT = 0;
  private static final int METHODID_INFORM_SERVER_OF_TAKEN_MEDICATIONS = 1;
  private static final int METHODID_GENERATE_FILE = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final userImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(userImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_TREATMENT:
          serviceImpl.getTreatment((com.andreea.grpc.User.Empty) request,
              (io.grpc.stub.StreamObserver<com.andreea.grpc.User.TreatmentRequest>) responseObserver);
          break;
        case METHODID_INFORM_SERVER_OF_TAKEN_MEDICATIONS:
          serviceImpl.informServerOfTakenMedications((com.andreea.grpc.User.Medication) request,
              (io.grpc.stub.StreamObserver<com.andreea.grpc.User.APIResponse>) responseObserver);
          break;
        case METHODID_GENERATE_FILE:
          serviceImpl.generateFile((com.andreea.grpc.User.TreatmentRequest) request,
              (io.grpc.stub.StreamObserver<com.andreea.grpc.User.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class userBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    userBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.andreea.grpc.User.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("user");
    }
  }

  private static final class userFileDescriptorSupplier
      extends userBaseDescriptorSupplier {
    userFileDescriptorSupplier() {}
  }

  private static final class userMethodDescriptorSupplier
      extends userBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    userMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (userGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new userFileDescriptorSupplier())
              .addMethod(getGetTreatmentMethod())
              .addMethod(getInformServerOfTakenMedicationsMethod())
              .addMethod(getGenerateFileMethod())
              .build();
        }
      }
    }
    return result;
  }
}
