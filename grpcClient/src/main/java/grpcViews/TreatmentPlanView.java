package grpcViews;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.andreea.grpc.User.APIResponse;
import com.andreea.grpc.User.Medication;
import com.andreea.grpc.User.TreatmentRequest;
import com.andreea.grpc.userGrpc;
import com.andreea.grpc.userGrpc.userBlockingStub;

import grpcClient.GrpcClient;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import javax.swing.ScrollPaneConstants;
import java.awt.SystemColor;
import javax.swing.JButton;

public class TreatmentPlanView extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel tabel1;
	private JScrollPane scrollPane_1;
	private JLabel lblNewLabel_1;
	private JLabel lblTimer;
	private Date currentDate;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TreatmentRequest request =null;
					TreatmentPlanView frame = new TreatmentPlanView(request);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @throws DocumentException 
	 * @throws FileNotFoundException 
	 */
	public TreatmentPlanView(TreatmentRequest request, userBlockingStub userStub){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 633, 427);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("MEDICAL DISPENSER APP");
		lblNewLabel.setForeground(new Color(255, 51, 51));
		lblNewLabel.setBackground(SystemColor.desktop);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel.setBounds(73, 30, 300, 32);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 116, 347, 229);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Medication", "Interval I", "Interval II", "Taken"
			}
		));
		scrollPane.setViewportView(table);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setRowHeaderView(scrollPane_1);
		
		lblNewLabel_1 = new JLabel("Current Time: ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(26, 83, 116, 23);
		contentPane.add(lblNewLabel_1);
		
		lblTimer = new JLabel("New label");
		lblTimer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTimer.setBounds(139, 78, 234, 32);
		contentPane.add(lblTimer);
		
		
		tabel1 = (DefaultTableModel) table.getModel();
		currentDate = new Date();
		
		for(Medication m : request.getMedicationList()) {
			tabel1.addRow(new Object[] {m.getName(), m.getInterval1(), m.getInterval2(), m.getTaken()});
			if(m.getInterval2() <= currentDate.getHours()) {
				APIResponse response = userStub.informServerOfTakenMedications(m);
				System.out.println(response.getResponsemessage());
			}
		}
		
		int minutes=50;
		int hour=2;
		
		Timer timer = new Timer(1000, new ActionListener() {
			int count=0;
			@Override
			public void actionPerformed(ActionEvent e) {
			Date currentDate= new Date();
			lblTimer.setText(currentDate.toString());
			if(currentDate.getHours()==hour && currentDate.getMinutes()==minutes && count==0) {
				generateFile(request, currentDate);
				count++;
			}
			}
		});
		
		JButton btnDelete = new JButton("Take medication!");
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel tblModel = (DefaultTableModel) table.getModel();
				int index = table.getSelectedRow();
				if(table.getSelectedRowCount()==1) {
					String interval1 = table.getValueAt(table.getSelectedRow(), 1).toString();
					String interval2 = table.getValueAt(table.getSelectedRow(), 2).toString();
					
					int firstInterval = Integer.parseInt(interval1);
					int secondInterval = Integer.parseInt(interval2);
					
					if(firstInterval <= currentDate.getHours() && secondInterval > currentDate.getHours()) {
						
					tblModel.removeRow(table.getSelectedRow());
					
					request.getMedication(index).getName();
					Medication.Builder med= request.getMedication(index).newBuilder().setTaken(true).setName(request.getMedication(index).getName());
					APIResponse response = userStub.informServerOfTakenMedications(med.build());
					System.out.println(response.getResponsemessage());
					
					}else {
						JOptionPane.showMessageDialog(btnDelete,"Not a good interval!", "ERROR", JOptionPane.ERROR_MESSAGE);

					}
				}else {
					if(table.getRowCount()==0) {
						JOptionPane.showMessageDialog(btnDelete,"Table is empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
					}else {
						JOptionPane.showMessageDialog(btnDelete,"Select ONE row, please!", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnDelete.setBounds(418, 119, 172, 47);
		contentPane.add(btnDelete);
		
		timer.start();
		
	}
	
	public void generateFile(TreatmentRequest request, Date currentDate){
		Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 1);
        currentDate = c.getTime();
        c.setTime(currentDate);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
		try {
		      File myObj = new File("medicalPlan.txt");
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		  
	 try {
	      FileWriter myWriter = new FileWriter("medicalPlan.txt");
	      myWriter.write("Medical plan for "+day+"."+month+"."+year+"\n\n");
	      
	      for(int i=0; i< request.getMedicationCount(); i++) {
	    	  myWriter.write((i+1)+"."+request.getMedication(i).getName()+"--> "+request.getMedication(i).getInterval1()+" - "+request.getMedication(i).getInterval2());
	    	  myWriter.write("\n");
	      }
	      
	      myWriter.close();
	      System.out.println("Successfully wrote to the file.");
	    } catch (IOException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
	}
}
