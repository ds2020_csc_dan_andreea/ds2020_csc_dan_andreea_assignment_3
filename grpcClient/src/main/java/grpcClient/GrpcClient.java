package grpcClient;

import java.io.FileNotFoundException;

import com.andreea.grpc.User.Medication;
import com.andreea.grpc.User.TreatmentRequest;
import com.andreea.grpc.userGrpc;
import com.andreea.grpc.userGrpc.userBlockingStub;
import com.andreea.grpc.userGrpc.userStub;
import com.itextpdf.text.DocumentException;

import grpcViews.TreatmentPlanView;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClient{

	public static void main(String[] args) throws FileNotFoundException, DocumentException {
		
		//example
		/*ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		//stubs - generate from proto
		
		userBlockingStub userStub = userGrpc.newBlockingStub(channel);
		
		LoginRequest loginrequest = LoginRequest.newBuilder().setUsername("RAM").setPassword("RAu").build();
		APIResponse response = userStub.login(loginrequest);
		System.out.println(response.getResponsemessage());

		TreatmentPlanView tview = new TreatmentPlanView();
		tview.setVisible(true);
		*/
		
		//my code
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
		userBlockingStub userStub = userGrpc.newBlockingStub(channel);
		TreatmentRequest treatment2 = userStub.getTreatment(null);
		
		TreatmentPlanView treatment = new TreatmentPlanView(treatment2, userStub);
		treatment.setVisible(true);
		
	}
}
